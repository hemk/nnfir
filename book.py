import six.moves.cPickle as pickle
import gzip
import numpy as np
from matplotlib import pyplot as plt


def encode_label(j):
    e = np.zeros((10, 1))
    e[j] = 1.0
    return e


def shape_data(data, encode=True):
    features = [np.reshape(x, (784, 1)) for x in data[0]]

    labels = [encode_label(y) for y in data[1]]

    return zip(features, labels)


def load_data():
    with gzip.open('mnist.pkl.gz', 'rb') as f:
        u = pickle._Unpickler(f)
        u.encoding = 'latin1'
        train_data, validation_data, test_data = u.load()

    return shape_data(train_data), shape_data(test_data)


def average_digit(data, digit):
    filtered_data = [x[0] for x in data if np.argmax(x[1]) == digit]
    filtered_array = np.asarray(filtered_data)
    return np.average(filtered_array, axis=0)


def print_average_digit(data, digit):
    # Creating an average digit:
    avg_digit = average_digit(data, digit)
    img = (np.reshape(avg_digit, (28, 28)))
    plt.imshow(img)
    plt.show()


train, test = load_data()
